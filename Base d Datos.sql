CREATE TABLE tiempo_registro (
  fecha DATE,
  inicio TIME,
  fin TIME,
  interrupcion TIME,
  a_tiempo BOOLEAN,
  actividad VARCHAR(255),
  comentarios VARCHAR(255),
  cu INT
);


CREATE TABLE calendario_trabajos (
  id INT NOT NULL AUTO_INCREMENT,
  numero INT NOT NULL,
  titulo VARCHAR(255) NOT NULL,
  proceso VARCHAR(255) NOT NULL,
  fecha_estimada DATE NOT NULL,
  fecha_real DATE,
  tiempo_estimado TIME NOT NULL,
  tiempo_real TIME,
  avance FLOAT,
  PRIMARY KEY (id)
);

CREATE TABLE detalles_trabajo (
  id_trabajo INT NOT NULL,
  descripcion VARCHAR(255),
  unidades INT NOT NULL,
  velocidad INT NOT NULL,
  velocidad_maxima INT,
  velocidad_minima INT,
  PRIMARY KEY (id_trabajo),
  FOREIGN KEY (id_trabajo) REFERENCES calendario_trabajos (id) ON DELETE CASCADE
);

CREATE TABLE estimacion_tamaño (
  idPrograma int not null,
  programa VARCHAR(255) NOT NULL,
  loc INT NOT NULL,
  func_estimadas INT NOT NULL,
  min INT NOT NULL,
  med INT NOT NULL,
  max INT NOT NULL,
  PRIMARY KEY (idPrograma)
);


CREATE TABLE presupuesto_semanal_tiempo (
  semana INT NOT NULL,
  tarea VARCHAR(255) NOT NULL,
  tiempo_estimado INT NOT NULL,
  PRIMARY KEY (semana, tarea)
);